﻿using ElasticSearchNestConnector;
using MailLib;
using MailLib.Models;
using MailsToElastic.Models;
using MimeKit;
using Newtonsoft.Json.Linq;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MailsToElastic
{
    public class MailsToElasticConnector
    {
        private clsLogStates logStates = new clsLogStates();

        public async Task<GetMailsResult> GetMails(GetMailsRequest request)
        {
            var taskResult = await Task.Run<GetMailsResult>(() =>
            {
                var result = new GetMailsResult
                {
                    Result = logStates.LogState_Success.Clone()
                };

                request.MessageOutput?.OutputInfo($"Get existing mails. Server: {request.ElasticServer}, Port: {request.ElasticPort}, Index: {request.ElasticIndex}");
                var dbConnector = new clsUserAppDBSelector(request.ElasticServer, request.ElasticPort, request.ElasticIndex.ToLower(), 5000, Guid.NewGuid().ToString());
                var query = "*";
                if (request.DateRange != null)
                {
                    query = $"Date:[{request.DateRange.Start.ToString("yyyy-MM-ddTHH:mm:ss")} TO {request.DateRange.End.ToString("yyyy-MM-ddTHH:mm:ss")}]";
                }
                if (!string.IsNullOrEmpty(request.SearchText))
                {
                    if (query == "*")
                    {
                        query = $"{request.SearchText}";
                    }
                    else
                    {
                        query += $" AND {request.SearchText}";
                    }
                }
                var mails = dbConnector.GetData_Documents(5000, request.ElasticIndex, request.ElasticType, query);

                if (!mails.IsOK)
                {
                    result.Result = logStates.LogState_Error.Clone();
                    return result;
                }

                result.Mails = mails.Documents.Select(mail => new MailItem(mail.Dict)).ToList();
                request.MessageOutput?.OutputInfo($"Found {result.Mails.Count}");
                return result;
            });

            return taskResult;

        }

        public async Task<GetMailsResult> GetMails(GetMailsRequest request, string scrollId, int page, int pageSize)
        {
            var taskResult = await Task.Run<GetMailsResult>(() =>
            {
                var result = new GetMailsResult
                {
                    Result = logStates.LogState_Success.Clone()
                };

                request.MessageOutput?.OutputInfo($"Get existing mails. Server: {request.ElasticServer}, Port: {request.ElasticPort}, Index: {request.ElasticIndex}");
                var dbConnector = new clsUserAppDBSelector(request.ElasticServer, request.ElasticPort, request.ElasticIndex.ToLower(), 5000, Guid.NewGuid().ToString());
                var query = "*";
                if (request.DateRange != null)
                {
                    query = $"Date:[{request.DateRange.Start.ToString("yyyy-MM-ddTHH:mm:ss")} TO {request.DateRange.End.ToString("yyyy-MM-ddTHH:mm:ss")}]";
                }
                if (!string.IsNullOrEmpty(request.SearchText))
                {
                    if (query == "*")
                    {
                        query = $"{request.SearchText}";
                    }
                    else
                    {
                        query += $" AND {request.SearchText}";
                    }
                }
                var mails = dbConnector.GetData_Documents(pageSize, request.ElasticIndex, request.ElasticType, query, page, scrollId);

                if (!mails.IsOK)
                {
                    result.Result = logStates.LogState_Error.Clone();
                    return result;
                }

                result.Mails = mails.Documents.Select(mail => new MailItem(mail.Dict)).ToList();
                result.Page = page;
                result.Total = mails.TotalCount;
                result.ScrollId = mails.ScrollId;
                request.MessageOutput?.OutputInfo($"Found {result.Mails.Count}");
                return result;
            });

            return taskResult;
        }

        public async Task<ImportMailsResult> ImportImapMails(ImportMailsRequest<ImapLoginRequest> loginRequest, ImapGetMailsRequest getMailsRequest, CancellationToken cancellationToken)
        {
            var mailConnector = new IMAPConnector();
            mailConnector.SaveMails += MailConnector_SaveMails;
            var loginResult = await mailConnector.Login(loginRequest.MailLoginRequest);

            var result = new ImportMailsResult
            {
                Result = logStates.LogState_Success.Clone()
            };

            if (!loginResult.IsOk)
            {
                result.Result = logStates.LogState_Error.Clone();
                result.Result.Additional1 = loginResult.Message;
                return result;
            }

            getMailsRequest.MessageOutput?.OutputInfo("Check Attachment Store-Path");
            if (!Directory.Exists(loginRequest.AttachmentStorePath))
            {
                Directory.CreateDirectory(loginRequest.AttachmentStorePath);
                getMailsRequest.MessageOutput?.OutputInfo($"Created Attachment Store-Path {loginRequest.AttachmentStorePath}.");
            }

            DateRange range = null;
            if (getMailsRequest.Start != null && getMailsRequest.End != null)
            {
                range = new DateRange
                {
                    Start = getMailsRequest.Start.Value,
                    End = getMailsRequest.End.Value
                };
            }
            var getMailsFromESRequest = new GetMailsRequest
            {
                ElasticIndex = loginRequest.ElasticIndex,
                ElasticPort = loginRequest.ElasticPort,
                ElasticServer = loginRequest.ElasticServer,
                ElasticType = loginRequest.ElasticType,
                MessageOutput = getMailsRequest.MessageOutput,
                DateRange = range
            };

            var getMailsFromEsResult = await GetMails(getMailsFromESRequest);
            if (cancellationToken.IsCancellationRequested)
            {
                getMailsRequest.MessageOutput?.OutputInfo("Cancelled by User.");
                return result;
            }

            result.Result = getMailsFromEsResult.Result;

            if (result.Result.GUID == logStates.LogState_Error.GUID)
            {
                result.Result.Additional1 = "Error while loading Mails from Es-Index";
                return result;
            }

            var getMailsResult = await mailConnector.GetMailsOfAllFolders(getMailsRequest, 
                loginRequest.ElasticServer, 
                loginRequest.ElasticPort, 
                loginRequest.ElasticIndex, 
                loginRequest.ElasticType,
                loginRequest.AttachmentStorePath,
                getMailsFromEsResult.Mails.ToList<object>(),
                cancellationToken);

            getMailsRequest.MessageOutput?.OutputInfo($"Added {getMailsResult.Messages.Count} Mails");

            var dbConnector = new clsUserAppDBSelector(loginRequest.ElasticServer, loginRequest.ElasticPort, loginRequest.ElasticIndex.ToLower(), 5000, Guid.NewGuid().ToString());
            var dbWriter = new clsUserAppDBUpdater(dbConnector);

            var mails = getMailsResult.Messages;


            await mailConnector.Logout();

            return result;
        }

        private bool MailConnector_SaveMails(List<MimeMessage> mimeMessages, IMessageOutput messageOutput, string elasticServer, int elasticPort, string elasticIndex, string elasticType, string attachementStorePath, List<object> mails)
        {
            var ixLastInbox = mimeMessages.Count - 1;
            var mailItems = mails.Select(mail => (MailItem)mail).ToList();
            var dbReader = new clsUserAppDBSelector(elasticServer, elasticPort, elasticIndex, 5000, Guid.NewGuid().ToString());
            var dbWriter = new clsUserAppDBUpdater(dbReader);
            var result = true;
            if (mimeMessages.Any())
            {
                messageOutput?.OutputInfo($"Import Mails...");

                

                var documentList = new List<clsAppDocuments>();
                for (var jx = 0; jx < mimeMessages.Count; jx++)
                {
                    var message = mimeMessages[jx];
                    if (mailItems.Any(msg => msg.IsEqual(message)))
                    {
                        continue;
                    }
                    var dict = new Dictionary<string, object>();

                    dict.Add(nameof(message.Headers), message.Headers);
                    dict.Add(nameof(message.Bcc), message.Bcc);
                    dict.Add(nameof(message.HtmlBody), message.HtmlBody);
                    dict.Add(nameof(message.Cc), message.Cc);
                    dict.Add(nameof(message.Date), message.Date);
                    dict.Add(nameof(message.From), message.From);
                    dict.Add(nameof(message.Importance), message.Importance);
                    dict.Add(nameof(message.InReplyTo), message.InReplyTo);
                    var messageId = message.MessageId;
                    if (string.IsNullOrEmpty(messageId))
                    {
                        messageId = $"custom:{Guid.NewGuid()}";
                    }
                    dict.Add(nameof(message.MessageId), messageId);
                    dict.Add(nameof(message.MimeVersion), message.MimeVersion);
                    dict.Add(nameof(message.Priority), message.Priority);
                    dict.Add(nameof(message.ReplyTo), message.ReplyTo);
                    dict.Add(nameof(message.ResentBcc), message.ResentBcc);
                    dict.Add(nameof(message.ResentCc), message.ResentCc);
                    dict.Add(nameof(message.ResentDate), message.ResentDate);
                    dict.Add(nameof(message.ResentFrom), message.ResentFrom);
                    dict.Add(nameof(message.ResentMessageId), message.ResentMessageId);
                    dict.Add(nameof(message.ResentReplyTo), message.ResentReplyTo);
                    dict.Add(nameof(message.ResentSender), message.ResentSender);
                    dict.Add(nameof(message.ResentTo), message.ResentTo);
                    dict.Add(nameof(message.Sender), message.Sender);
                    dict.Add(nameof(message.Subject), message.Subject);
                    dict.Add(nameof(message.TextBody), message.TextBody);
                    dict.Add(nameof(message.To), message.To);
                    dict.Add(nameof(message.XPriority), message.XPriority);
                    dict.Add(nameof(MailItem.Folder), jx <= ixLastInbox ? "Inbox" : "Send");

                    var document = new clsAppDocuments
                    {
                        Id = messageId,
                        Dict = dict
                    };

                    foreach (var attachment in message.Attachments)
                    {
                        attachment.ContentId = Guid.NewGuid().ToString();
                        var subFolder1 = attachment.ContentId.Substring(0, 2);
                        var subFolder2 = attachment.ContentId.Substring(2, 2);
                        var path = Path.Combine(attachementStorePath, subFolder1, subFolder2);

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var filePath = Path.Combine(path, attachment.ContentId);
                        using (var stream = File.Create(filePath))
                        {
                            if (attachment is MessagePart)
                            {
                                var part = (MessagePart)attachment;

                                part.Message.WriteTo(stream);
                            }
                            else
                            {
                                var part = (MimePart)attachment;

                                part.Content.DecodeTo(stream);
                            }

                        }

                    }

                    var listAttachments = message.Attachments.Select(attachm =>
                    {
                        var dictAtt = new Dictionary<string, object>();
                        dictAtt.Add(nameof(attachm.Headers), attachm.Headers);
                        dictAtt.Add(nameof(attachm.ContentType), attachm.ContentType);
                        dictAtt.Add(nameof(attachm.ContentDisposition), attachm.ContentDisposition);
                        dictAtt.Add(nameof(attachm.ContentId), attachm.ContentId);
                        return dictAtt;
                    });
                    dict.Add(nameof(message.Attachments), listAttachments);

                    documentList.Add(document);
                }


                if (documentList.Any())
                {

                    var resultDocs = dbWriter.SaveDoc(documentList, elasticType);

                    if (resultDocs.GUID == logStates.LogState_Error.GUID)
                    {
                        result = false;
                        return result;
                    }

                    messageOutput?.OutputInfo($"Imported Mails.");
                }

            }
            return result;
        }
    }
}