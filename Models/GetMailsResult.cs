﻿using MimeKit;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailsToElastic.Models
{
    public class GetMailsResult
    {
        public List<MailItem> Mails {get; set;}
        public clsOntologyItem Result { get; set; }

        public int Page { get; set; } = -1;
        public long Total { get; set; } = -1;

        public string ScrollId { get; set; }

    }
}
