﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailsToElastic.Models
{
    public class ImportMailsResult
    {
        public clsOntologyItem Result { get; set; }
        public int CountImported { get; set; }
    }
}
