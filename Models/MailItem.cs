﻿using MimeKit;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailsToElastic.Models
{
    public class Header
    {
        public int Offset { get; set; }
        public string Field { get; set; }
        public int Id { get; set; }
        public string RawField { get; set; }
        public string RawValue { get; set; }
        public string Value { get; set; }

        public Header(JObject header)
        {
            if (header != null)
            {
                var proxyItem = Newtonsoft.Json.JsonConvert.DeserializeObject<Header>(header.ToString());
                foreach (var prop in proxyItem.GetType().GetProperties())
                {
                    prop.SetValue(this, prop.GetValue(proxyItem));
                }
            }
        }

    }

    public class MailAddress
    {
        public string Address { get; set; }
        public bool IsInternational { get; set; }
        public Encoding Encoding { get; set; }
        public string Name { get; set; }

        public MailAddress(JObject address)
        {
            Address = (string)address.GetValue(nameof(Address));
            JToken token;
            if (address.TryGetValue(nameof(IsInternational), out token))
            {
                IsInternational = (bool)token;
            }
            
            Name = (string)address.GetValue(nameof(Name));
            Encoding = Newtonsoft.Json.JsonConvert.DeserializeObject<Encoding>(address.GetValue(nameof(Encoding)).ToString());

        }
    }

    public class MimeVersion
    {
        public int? Major { get; set; }
        public int? Minor { get; set; }
        public int? Build { get; set; }
        public int? Revision { get; set; }
        public int? MajorRevision { get; set; }
        public int? MinorRevision { get; set; }

        public MimeVersion(JObject item)
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                foreach (JProperty subItem in item.Children())
                {
                    if (subItem.Name == prop.Name)
                    {
                        var value = (int?)subItem.Value;
                        if (value != null && ((int)value) != -1)
                        {
                            prop.SetValue(this, (int)value);
                        }
                    }
                    
                }
                //foreach (JObject item in itemArray)
                //{
                //    var value = item.GetValue(nameof(prop.Name));
                //    if (value != null && ((int)value) != -1)
                //    {
                //        prop.SetValue(this, (int)value);
                //    }
                //}


            }
        }
    }

    public class EncoderFallback
    {
        public int MaxCharCount { get; set; }
    }

    public class DecoderFallback
    {
        public int MaxCharCount { get; set; }
    }

    public class Encoding
    {
        public string BodyName { get; set; }
        public string EncodingName { get; set; }
        public string HeaderName { get; set; }
        public string WebName { get; set; }
        public int WindowsCodePage { get; set; }
        public bool IsBrowserDisplay { get; set; }
        public bool IsBrowserSave { get; set; }
        public bool IsMailNewsDisplay { get; set; }
        public bool IsMailNewsSave { get; set; }
        public bool IsSingleByte { get; set; }
        public EncoderFallback EncoderFallback { get; set; }
        public DecoderFallback DecoderFallback { get; set; }
        public bool IsReadOnly { get; set; }
        public int CodePage { get; set; }
    }

    public class Parameter
    {
        public string Name { get; set; }
        public Encoding Encoding { get; set; }
        public int EncodingMethod { get; set; }
        public string Value { get; set; }
    }

    public class ContentType
    {
        public string MediaType { get; set; }
        public string MediaSubtype { get; set; }
        public List<Parameter> Parameters { get; set; }
        public string MimeType { get; set; }
        public string Name { get; set; }

        public ContentType(JObject contentType)
        {
            if (contentType != null)
            {
                MediaType = contentType.GetValue(nameof(MediaType)).ToString();
                MediaSubtype = contentType.GetValue(nameof(MediaSubtype)).ToString();
                MimeType = contentType.GetValue(nameof(MimeType)).ToString();
                Name = contentType.GetValue(nameof(Name))?.ToString();
            }
        }
    }

    public class EncoderFallback2
    {
        public int MaxCharCount { get; set; }
    }

    public class DecoderFallback2
    {
        public int MaxCharCount { get; set; }
    }

    public class Encoding2
    {
        public string BodyName { get; set; }
        public string EncodingName { get; set; }
        public string HeaderName { get; set; }
        public string WebName { get; set; }
        public int WindowsCodePage { get; set; }
        public bool IsBrowserDisplay { get; set; }
        public bool IsBrowserSave { get; set; }
        public bool IsMailNewsDisplay { get; set; }
        public bool IsMailNewsSave { get; set; }
        public bool IsSingleByte { get; set; }
        public EncoderFallback2 EncoderFallback { get; set; }
        public DecoderFallback2 DecoderFallback { get; set; }
        public bool IsReadOnly { get; set; }
        public int CodePage { get; set; }
    }

    public class Parameter2
    {
        public string Name { get; set; }
        public Encoding2 Encoding { get; set; }
        public int EncodingMethod { get; set; }
        public string Value { get; set; }
    }

    public class ContentDisposition
    {
        public string Disposition { get; set; }
        public bool IsAttachment { get; set; }
        public List<Parameter2> Parameters { get; set; }
        public string FileName { get; set; }

        public ContentDisposition(JObject contentDisposition)
        {
            if (contentDisposition != null)
            {
                Disposition = contentDisposition.GetValue(nameof(Disposition)).ToString();
                FileName = contentDisposition.ContainsKey(nameof(FileName)) ? contentDisposition.GetValue(nameof(FileName)).ToString() : "";
                Parameters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Parameter2>>(contentDisposition.GetValue(nameof(Parameters)).ToString());
                IsAttachment = (bool)contentDisposition.GetValue(nameof(IsAttachment));
            }
        }

    }

    public class Attachment
    {
        public List<Header> Headers { get; set; }
        public ContentType ContentType { get; set; }
        public ContentDisposition ContentDisposition { get; set; }
        public string ContentId { get; set; }

        public Attachment(JObject attachment)
        {
            Headers = new List<Header>();
            var headers = (JArray)attachment.GetValue(nameof(Headers));
            if (headers != null)
            {
                foreach (JObject item in headers)
                {
                    Headers.Add(new Header(item));
                }
            }


            var contentType = (JObject)attachment.GetValue(nameof(ContentType));
            ContentType = new ContentType(contentType);

            var contentDisposition = (JObject)attachment.GetValue(nameof(ContentDisposition));
            ContentDisposition = new ContentDisposition(contentDisposition);

            ContentId = attachment.GetValue(nameof(ContentId)).ToString();
        }
    }

    public class MailItem
    {
        public string Folder { get; set; }
        public List<Header> Headers { get; set; }
        public string HtmlBody { get; set; }
        public MessageImportance Importance { get; set; }
        //
        // Zusammenfassung:
        //     Get or set the value of the Priority header.
        //
        // Ausnahmen:
        //   T:System.ArgumentOutOfRangeException:
        //     value is not a valid MimeKit.MessagePriority.
        //
        // Hinweise:
        //     Gets or sets the value of the Priority header.
        public MessagePriority Priority { get; set; }
        //
        // Zusammenfassung:
        //     Get or set the value of the X-Priority header.
        //
        // Ausnahmen:
        //   T:System.ArgumentOutOfRangeException:
        //     value is not a valid MimeKit.MessagePriority.
        //
        // Hinweise:
        //     Gets or sets the value of the X-Priority header.
        public XMessagePriority XPriority { get; set; }
        //
        // Zusammenfassung:
        //     Gets or sets the address in the Sender header.
        //
        // Hinweise:
        //     The sender may differ from the addresses in MimeKit.MimeMessage.From if the message
        //     was sent by someone on behalf of someone else.
        public MailAddress Sender { get; set; }
        //
        // Zusammenfassung:
        //     Gets or sets the address in the Resent-Sender header.
        //
        // Hinweise:
        //     The resent sender may differ from the addresses in MimeKit.MimeMessage.ResentFrom
        //     if the message was sent by someone on behalf of someone else.
        public MailAddress ResentSender { get; set; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the From header.
        //
        // Hinweise:
        //     The "From" header specifies the author(s) of the message.
        //     If more than one MimeKit.MailAddress is added to the list of "From" addresses,
        //     the MimeKit.MimeMessage.Sender should be set to the single MimeKit.MailAddress
        //     of the personal actually sending the message.
        public List<MailAddress> From { get; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the Resent-From header.
        //
        // Hinweise:
        //     The "Resent-From" header specifies the author(s) of the messagebeing resent.
        //     If more than one MimeKit.MailAddress is added to the list of "Resent-From"
        //     addresses, the MimeKit.MimeMessage.ResentSender should be set to the single MimeKit.MailAddress
        //     of the personal actually sending the message.
        public List<MailAddress> ResentFrom { get; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the Reply-To header.
        //
        // Hinweise:
        //     When the list of addresses in the Reply-To header is not empty, it contains the
        //     address(es) where the author(s) of the message prefer that replies be sent.
        //     When the list of addresses in the Reply-To header is empty, replies should be
        //     sent to the mailbox(es) specified in the From header.
        public List<MailAddress> ReplyTo { get; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the Resent-Reply-To header.
        //
        // Hinweise:
        //     When the list of addresses in the Resent-Reply-To header is not empty, it contains
        //     the address(es) where the author(s) of the resent message prefer that replies
        //     be sent.
        //     When the list of addresses in the Resent-Reply-To header is empty, replies should
        //     be sent to the mailbox(es) specified in the Resent-From header.
        public List<MailAddress> ResentReplyTo { get; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the To header.
        //
        // Hinweise:
        //     The addresses in the To header are the primary recipients of the message.
        public List<MailAddress> To { get; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the Resent-To header.
        //
        // Hinweise:
        //     The addresses in the Resent-To header are the primary recipients of the message.
        public List<MailAddress> ResentTo { get; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the Cc header.
        //
        // Hinweise:
        //     The addresses in the Cc header are secondary recipients of the message and are
        //     usually not the individuals being directly addressed in the content of the message.
        public List<MailAddress> Cc { get; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the Resent-Cc header.
        //
        // Hinweise:
        //     The addresses in the Resent-Cc header are secondary recipients of the message
        //     and are usually not the individuals being directly addressed in the content of
        //     the message.
        public List<MailAddress> ResentCc { get; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the Bcc header.
        //
        // Hinweise:
        //     Recipients in the Blind-Carpbon-Copy list will not be visible to the other recipients
        //     of the message.
        public List<MailAddress> Bcc { get; }
        //
        // Zusammenfassung:
        //     Gets the list of addresses in the Resent-Bcc header.
        //
        // Hinweise:
        //     Recipients in the Resent-Bcc list will not be visible to the other recipients
        //     of the message.
        public List<MailAddress> ResentBcc { get; }
        //
        // Zusammenfassung:
        //     Gets or sets the subject of the message.
        //
        // Ausnahmen:
        //   T:System.ArgumentNullException:
        //     value is null.
        //
        // Hinweise:
        //     The Subject is typically a short string denoting the topic of the message.
        //     Replies will often use "Re: " followed by the Subject of the original message.
        public string Subject { get; set; }
        //
        // Zusammenfassung:
        //     Gets or sets the date of the message.
        //
        // Hinweise:
        //     If the date is not explicitly set before the message is written to a stream,
        //     the date will default to the exact moment when it is written to said stream.
        public DateTimeOffset Date { get; set; }
        //
        // Zusammenfassung:
        //     Gets or sets the Resent-Date of the message.
        //
        // Hinweise:
        //     Gets or sets the Resent-Date of the message.
        public DateTimeOffset ResentDate { get; set; }
        //
        // Zusammenfassung:
        //     Gets or sets the list of references to other messages.
        //
        // Hinweise:
        //     The References header contains a chain of Message-Ids back to the original message
        //     that started the thread.
        public MessageIdList References { get; }
        //
        // Zusammenfassung:
        //     Gets or sets the Message-Id that this message is in reply to.
        //
        // Ausnahmen:
        //   T:System.ArgumentException:
        //     value is improperly formatted.
        //
        // Hinweise:
        //     If the message is a reply to another message, it will typically use the In-Reply-To
        //     header to specify the Message-Id of the original message being replied to.
        public string InReplyTo { get; set; }
        //
        // Zusammenfassung:
        //     Gets or sets the message identifier.
        //
        // Ausnahmen:
        //   T:System.ArgumentNullException:
        //     value is null.
        //
        //   T:System.ArgumentException:
        //     value is improperly formatted.
        //
        // Hinweise:
        //     The Message-Id is meant to be a globally unique identifier for a message.
        //     MimeKit.Utils.MimeUtils.GenerateMessageId can be used to generate this value.
        public string MessageId { get; set; }
        //
        // Zusammenfassung:
        //     Gets or sets the Resent-Message-Id header.
        //
        // Ausnahmen:
        //   T:System.ArgumentNullException:
        //     value is null.
        //
        //   T:System.ArgumentException:
        //     value is improperly formatted.
        //
        // Hinweise:
        //     The Resent-Message-Id is meant to be a globally unique identifier for a message.
        //     MimeKit.Utils.MimeUtils.GenerateMessageId can be used to generate this value.
        public string ResentMessageId { get; set; }
        //
        // Zusammenfassung:
        //     Gets or sets the MIME-Version.
        //
        // Ausnahmen:
        //   T:System.ArgumentNullException:
        //     value is null.
        //
        // Hinweise:
        //     The MIME-Version header specifies the version of the MIME specification that
        //     the message was created for.
        public MimeVersion MimeVersion { get; set; }
        public List<Attachment> Attachments { get; set; }
        public bool IsRead { get; set; }

        public bool IsEqual(MimeMessage mimeMessage)
        {
            if (!string.IsNullOrEmpty(mimeMessage.MessageId) && !mimeMessage.MessageId.StartsWith("custom:"))
            {
                return (mimeMessage.MessageId == MessageId);
            }
            else
            {
                return (mimeMessage.Date == Date && mimeMessage.Subject == Subject);
            }
        }

        public MailItem(Dictionary<string, object> dictMailItem)
        {
            if (dictMailItem.ContainsKey(nameof(Folder)))
            {
                Folder = dictMailItem[nameof(Folder)].ToString();
            }
            Headers = new List<Header>();
            if (dictMailItem.ContainsKey(nameof(Headers)))
            {
                var headers = (JArray)dictMailItem[nameof(Headers)];
                foreach (JObject item in headers)
                {
                    Headers.Add(new Header(item));
                }
            }

            if (dictMailItem.ContainsKey("TextBody"))
            {
                HtmlBody = dictMailItem["TextBody"].ToString();
            }

            if (dictMailItem.ContainsKey(nameof(HtmlBody)))
            {
                HtmlBody = dictMailItem[nameof(HtmlBody)].ToString();
            }

            if (dictMailItem.ContainsKey(nameof(Importance)))
            {
                Importance = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageImportance>(dictMailItem[nameof(Importance)].ToString());
            }

            Attachments = new List<Attachment>();
            if (dictMailItem.ContainsKey(nameof(Attachments)))
            {
                var attachments = (JArray)dictMailItem[nameof(Attachments)];
                foreach (JObject item in attachments)
                {
                    Attachments.Add(new Attachment(item));
                }
                
            }

            if (dictMailItem.ContainsKey(nameof(Bcc)))
            {
                Bcc = new List<MailAddress>();
                var bcc = dictMailItem[nameof(Bcc)].ToString();
                var bccArray = (JArray)Newtonsoft.Json.JsonConvert.DeserializeObject(bcc);
                foreach (JObject jObject in bccArray)
                {
                    Bcc.Add(new MailAddress(jObject));
                }
            }

            if (dictMailItem.ContainsKey(nameof(this.Cc)))
            {
                Cc = new List<MailAddress>();
                var cc = dictMailItem[nameof(Cc)].ToString();
                var ccArray = (JArray)Newtonsoft.Json.JsonConvert.DeserializeObject(cc);
                foreach (JObject jObject in ccArray)
                {
                    Cc.Add(new MailAddress(jObject));
                }
                //Cc = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MailAddress>>(dictMailItem[nameof(Cc)].ToString());
            }

            if (dictMailItem.ContainsKey(nameof(this.Date)))
            {
                Date = (DateTime)dictMailItem[nameof(Date)];
            }

            if (dictMailItem.ContainsKey(nameof(this.From)))
            {
                From = new List<MailAddress>();
                var froms = (JArray)dictMailItem[nameof(From)];
                foreach (JObject item in froms)
                {
                    if (item != null)
                    {
                        var from = new MailAddress(item);
                        From.Add(from);
                    }
                    
                    
                }
                
            }

            if (dictMailItem.ContainsKey(nameof(this.InReplyTo)))
            {
                InReplyTo = dictMailItem[nameof(InReplyTo)].ToString();
            }

            if (dictMailItem.ContainsKey(nameof(this.MessageId)))
            {
                MessageId = dictMailItem[nameof(MessageId)].ToString();
            }

            if (dictMailItem.ContainsKey(nameof(this.MimeVersion)))
            {
                var mimeVersion = (JObject)dictMailItem[nameof(this.MimeVersion)];
                MimeVersion = new MimeVersion(mimeVersion);
            }

            if (dictMailItem.ContainsKey(nameof(this.Priority)))
            {
                Priority = Newtonsoft.Json.JsonConvert.DeserializeObject<MessagePriority>(dictMailItem[nameof(Priority)].ToString());
            }

            if (dictMailItem.ContainsKey(nameof(this.References)))
            {
                References = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageIdList>(dictMailItem[nameof(References)].ToString());
            }

            if (dictMailItem.ContainsKey(nameof(this.ReplyTo)))
            {
                ReplyTo = new List<MailAddress>();
                var addresses = (JArray)dictMailItem[nameof(ReplyTo)];
                foreach (JObject item in addresses)
                {
                    if (item != null)
                    {
                        var address = new MailAddress(item);
                        ReplyTo.Add(address);
                    }


                }
            }

            if (dictMailItem.ContainsKey(nameof(this.ResentBcc)))
            {
                ResentBcc = new List<MailAddress>();
                var addresses = (JArray)dictMailItem[nameof(ResentBcc)];
                foreach (JObject item in addresses)
                {
                    if (item != null)
                    {
                        var address = new MailAddress(item);
                        ResentBcc.Add(address);
                    }


                }
            }

            if (dictMailItem.ContainsKey(nameof(this.ResentCc)))
            {
                ResentCc = new List<MailAddress>();
                var addresses = (JArray)dictMailItem[nameof(ResentCc)];
                foreach (JObject item in addresses)
                {
                    if (item != null)
                    {
                        var address = new MailAddress(item);
                        ResentCc.Add(address);
                    }


                }
            }

            if (dictMailItem.ContainsKey(nameof(this.ResentDate)))
            {
                ResentDate = (DateTime)dictMailItem[nameof(ResentDate)];
            }

            if (dictMailItem.ContainsKey(nameof(this.ResentFrom)))
            {
                ResentFrom = new List<MailAddress>();
                var addresses = (JArray)dictMailItem[nameof(ResentFrom)];
                foreach (JObject item in addresses)
                {
                    if (item != null)
                    {
                        var address = new MailAddress(item);
                        ResentFrom.Add(address);
                    }


                }
            }

            if (dictMailItem.ContainsKey(nameof(this.ResentMessageId)))
            {
                ResentMessageId = dictMailItem[nameof(ResentMessageId)].ToString();
            }

            if (dictMailItem.ContainsKey(nameof(this.ResentReplyTo)))
            {
                ResentReplyTo = new List<MailAddress>();
                var addresses = (JArray)dictMailItem[nameof(ResentReplyTo)];
                foreach (JObject item in addresses)
                {
                    if (item != null)
                    {
                        var address = new MailAddress(item);
                        ResentReplyTo.Add(address);
                    }


                }
                
            }

            if (dictMailItem.ContainsKey(nameof(this.ResentSender)))
            {
                ResentSender = Newtonsoft.Json.JsonConvert.DeserializeObject<MailAddress>(dictMailItem[nameof(ResentSender)].ToString());
            }

            if (dictMailItem.ContainsKey(nameof(this.ResentTo)))
            {
                ResentTo = new List<MailAddress>();
                var addresses = (JArray)dictMailItem[nameof(ResentTo)];
                foreach (JObject item in addresses)
                {
                    if (item != null)
                    {
                        var address = new MailAddress(item);
                        ResentTo.Add(address);
                    }


                }
            }

            if (dictMailItem.ContainsKey(nameof(this.Sender)))
            {
                var sender = dictMailItem[nameof(Sender)].ToString();
                var senderObject = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(sender);
                Sender = new MailAddress(senderObject);

                //Sender = Newtonsoft.Json.JsonConvert.DeserializeObject<MailAddress>(dictMailItem[nameof(Sender)].ToString());
            }

            if (dictMailItem.ContainsKey(nameof(this.Subject)))
            {
                Subject = dictMailItem[nameof(Subject)].ToString();
            }

            if (dictMailItem.ContainsKey(nameof(this.To)))
            {
                To = new List<MailAddress>();
                var addresses = (JArray)dictMailItem[nameof(To)];
                foreach (JObject item in addresses)
                {
                    if (item != null)
                    {
                        var address = new MailAddress(item);
                        To.Add(address);
                    }


                }
                
            }

            if (dictMailItem.ContainsKey(nameof(this.XPriority)))
            {
                XPriority = Newtonsoft.Json.JsonConvert.DeserializeObject<XMessagePriority>(dictMailItem[nameof(XPriority)].ToString());
            }
        }
    }

    
}
