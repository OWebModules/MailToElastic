﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailLib.Models;

namespace MailsToElastic.Models
{
    public class ImportMailsRequest<TType>
    {
        public TType MailLoginRequest { get; set; }
        public string AttachmentStorePath { get; set; }
        public string ElasticServer { get; set; }
        public int ElasticPort { get; set; }
        public string ElasticIndex { get; set; }
        public string ElasticType { get; set; }
    }
}
