﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailsToElastic.Models
{
    public class GetMailsRequest
    {
        public string ElasticServer { get; set; }
        public int ElasticPort { get; set; }
        public string ElasticIndex { get; set; }
        public string ElasticType { get; set; }

        public DateRange DateRange { get; set; }

        public string SearchText { get; set; }

        public IMessageOutput MessageOutput { get; set; }
    }
}
